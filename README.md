Projeto para estágio pós-doutoral na Universidade Federal de Alagoas ([UFAL]
(http://www.ufal.edu.br/)), Brasil.

Projeto apresentado ao Programa de Pós-Graduação em Matemática da Universidade Federal de Alagoas-
[PPGMAT/UFAL](http://www.im.ufal.br/posgraduacao/posmat/index.php),
atendendo à chamada pública do [Edital nº 03/2016-PPGMAT/UFAL](http://www.im.ufal.br/posgraduacao/posmat/images/edital_03_2016_CR_P.pdf)
para a participação no processo seletivo destinado à formação de cadastro reserva para bolsista do Programa Nacional de Pós-Doutorado (PNPD/CAPES)
do PPGMAT/UFAL.
